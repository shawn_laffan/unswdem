#!/usr/bin/env python

"""
Convert all rasters in a workspace to point.
Causes massive files for the SRTM data, so avoid using it for that.
"""

import arcgisscripting
import os
import sys

#  raster and feature workspaces
r_workspace = r'D:\shawn\SRTM_V4\grid'
#r_workspace = r'D:\shawn\SRTM_V4\xx'
f_workspace = r'D:\shawn\SRTM_V4\point'

gp = arcgisscripting.create(9.3)

if (gp.workspace is None):
    gp.workspace = r_workspace
print gp.workspace

#rasters = gp.listrasters (os.path.join (r_workspace, 'srtm*'))
rasters = gp.listrasters ('srtm*')


#print rasters[0:10]

for raster in rasters:
    try:
        # Set local variables
        f_name = raster + '_p'
        out_feat = os.path.join (f_workspace, f_name) + '.shp'
        

        if not gp.exists (out_feat):
            print out_feat
            gp.RasterToPoint_conversion(raster, out_feat)
    
    except:
        # Print error message if an error occurs
        print gp.GetMessages()
        raise
