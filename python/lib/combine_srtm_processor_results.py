#!/usr/bin/env python

"""
Combine the results of a process_srtm.py run.
This is needed because multiple files can be output,
but these can each have a different coordinate system.
"""

import arcpy
# import arcpy.management as arcmgt
import os
import sys
import argparser


def get_args():
    parser = argparse.ArgumentParser(description='Ask for the outfile', prog='Input')
    parser.add_argument('out_file', type=str, help='out_file')
    parser.add_argument('globber', type=str, help='globber')
    parser.add_argument('-w', '--overwrite', type=str, help='overwrite', default=None)
    parser.add_argument('-s', '--workspace', type=str, help='set workspace', default='.')
    args = parser.parse_args()
    out_file = args.out_file
    globber = args.globber
    overwrite = args.overwrite
    workspace = args.workspace
    print('workspace is', workspace)
    return out_file, globber, overwrite, workspace


if __name__ == "__main__":
    try:
        out_file, globber, overwrite, workspace = get_args()
    except:
        print 'Input is not valid'
        sys.exit()

    arcpy.env.workspace = workspace
    #
    # out_file = arcpy.GetParameterAsText(0)
    # globber = arcpy.GetParameterAsText(1)
    # overwrite = arcpy.GetParameterAsText(2)

    # if not len(out_file):
    #     arcpy.AddError("Argument 'out_file' not provided")
    #     print
    #     arcpy.GetMessages()
    #     sys.exit()

    # set the workspace
    # if (arcpy.env.workspace is None):
    #     arcpy.env.workspace = '.'
    # print
    # "Workspace is", arcpy.env.workspace

    if overwrite:
        arcpy.env.overWriteOutput = True
    files = arcpy.ListFiles(globber)
    if not files:
        print 'no files'
        sys.exit()
    layers = []
    for file in files:
        pfx = os.path.splitext(file)[0]
        prj_file = pfx + '.prj'
        out_lyr = pfx + 'lyr'
        print 'the file name is', prj_file
        try:
            # ??MakeXYEventLayer_management
            arcpy.MakeXYEventLayer_management(file, 'x', 'y', out_lyr, prj_file)
            layers.append(out_lyr)
        except:
            print "Unable to process %s, skipping" % out_lyr
    # all layers
    layer_txt = ";".join(layers)

    # coord_sys = os.path.join (
    #    os.environ['ARCGISHOME'],
    #    r"Coordinate Systems\Geographic
    #  Coordinate Systems\World\WGS 1984.prj"
    #    )
    # coord_sys = r"Coordinate Systems\Projected Coordinate Systems\UTM\WGS 1984\Northern Hemisphere\WGS 1984 UTM Zone 1N.prj"
    # coordination system
    coord_sys = arcpy.SpatialReference("WGS 1984")
    arcpy.OutputCoordinateSystem = coord_sys
    # a shape file container
    tmp_name = arcpy.CreateScratchName("", "", data_type="shapefile")
    print "Merging"
    # merge previous all layers into the output layer (tmp_name)
    arcpy.Merge_management(layer_txt, tmp_name)

    #  get the fields
    fields = arcpy.ListFields(tmp_name)

    # search the fields list until reach x, then join everything after x as a string.
    i = 0
    while i<len(fields):
        if fields[i] == 'x':
            break
        i+=1
    flds = ';'.join(fields[i+1:])
    print "field names:" + flds

    #  now we need to remove any duplicates
    # because now overwrite? and the outfile is compulsory we don't need to check arcpy exists
    print "Dissolving"
    if overwrite and arcpy.Exists(out_file):
        try:
            arcpy.Delete_management(out_file)
        except:
            print 'something wrong when overwriting the outfile'
            raise
    arcpy.Dissolve_management(tmp_name, out_file, flds, "", multi_part="SINGLE_PART")

    #  formulae dictionary
    #  D*D etc are to avoid numeric domain issues with
    #  D**2 becoming -2**2 = -4 in the field calcs.
    #  Presumably this is because the code block is passed as a string,
    #  which is then interpeted as -1*2**2=-4, with ** having highest precedence.
    formulae = {'slope': '100 * math.atan ( math.sqrt (E * E + D * D))',
                'aspect': 'math.atan (E / D)',
                'longc': '100 * -2 * (A*D*D + C*E*E + B*D*E) / (D*D + E*E)',
                'crossc': '100 * -2 * (C*D*D + A*E*E + B*D*E) / (D*D + E*E)',
                }

    print "Calculating slope, aspect, longc and crossc"
    #  and now add the slopes etc.
    for fld_name in (formulae.keys()):
        arcpy.AddField_management(out_file, fld_name, 'DOUBLE')
        formula = 'def get_value(A, B, C, D, E, F):\n  import math\n  return %s' % formulae[fld_name]
        target = "get_value(A=!A!, B=!B!, C=!C!, D=!D!, E=!E!, F=!F!)"
        # print target, "\n", formula
        arcpy.CalculateField_management(out_file, fld_name, target, "PYTHON_9.3", formula)

    arcpy.AddMessage("Process complete")
