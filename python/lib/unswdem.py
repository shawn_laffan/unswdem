#!/usr/bin/env python
import numpy as np
import numpy.linalg as linalg
from math import tan, atan, cos, sin, pi, sqrt

morphclasses = ('peak', 'pit', 'ridge', 'valley', 'saddle', 'plane')

muval = {}
for morphclass in morphclasses:
    muval[morphclass] = 0
    
pi_div2 = pi / 2

class UNSWDEM:
    """
    Class to calculate UNSWDEM morphometric indices.
    """

    def __init__(self, radius=5, offset=(0,0)):
        # Set all the properties
        self.radius = radius     #  default window radius
        self.reset()
        self.offset = offset
        # self.x = np.array(x)- x_centre
        # self.y = np.array(y)- y_centre
        # self.z = np.array(z)


    def reset(self):

        self.coeffs = {}
        self.muval  = muval.copy()
        self.theta  = None
        self.x      = []
        self.y      = []
        self.z      = []
        self.offset = [0,0]
        self.centre = [None, None]
        self.axes   = [[None, None, None], [None, None, None]]
        self.r2     = None
        self.model  = None
        
        self.cmti   = 0

        self.dist     = None
        self.dist_ax1 = None
        self.dist_ax2 = None

        self.parallel   = False
        self.morphval   = None
        self.morphtype  = None
        self.morphclass = None
        
        self.as_ascii   = ""
        
        return

    def get_morphclasses (self):
        return morphclasses

    def set_data(self, x, y, z):
        self.x = np.array(x)
        self.y = np.array(y)
        self.z = np.array(z)

    def centre_coords (self, x_centre = 0, y_centre = 0):
        self.x -= x_centre
        self.y -= y_centre

    def get_dist_p2l (self, line_array, x0=0, y0=0):
        """ Distance from a point to a line.
        Arguments are set up for a formula like
        ax + by + c = 0
        """
        a = line_array[0]
        b = line_array[1]
        c = line_array[2]
        
        numer = abs (a * x0 + b * y0 + c)
        if a or b:
            denom = sqrt (a**2 + b**2)
        else:
            denom = 1
        
        ## two val version - assumes y slope is -1
        #numer = abs (a * x0 - y0 + c)
        #denom = sqrt (1 + a**2)
        
        dist = numer / denom
        
        return dist

    def generate_test_val(self, type='elliptic', x=0, y=0, rot=1):
        """
        Generates a value for a given x,y cordinate.
        The value deends on the type argument.
        """
        if type == 'elliptic':
            return x**2 + y**2 + rot * x * y
            #return x**2 + 2*x*y + y**2 + x + y
        elif type == 'hyperbolic':
            return y**2 - x**2 + rot * x * y
        elif type == 'parabolic':
            return x**2 + rot * x * y
        elif type == 'planar':
            return x + y
        elif type == 'flat':
            return 1
        else:
            raise "morphtype %s not valid" % type


    def data_to_ascii(self):
        x = self.x
        y = self.y
        z = self.z

        ascii = {}

        for i in (range (len(z))):
            val = "%3i" % (z[i])
            try:
                ascii[y[i]][x[i]] = val
            except:
                ascii[y[i]] = {}
                ascii[y[i]][x[i]] = val

        #print ascii
        #print ascii.keys()

        string = ""
        nullstring = "."
        radius = self.radius
        for ykey in (range (max(y.max(), radius), min(y.min(), -radius)-1, -1)):
            string += "%3i|" % ykey
            for xkey in (range (min(x.min(), -radius), max(x.max(), radius)+1)):
                try:
                    string += ascii[ykey][xkey] + " "
                except:
                    string += "%3s " % (nullstring)
            string += "\n"
        
        string += "%3s " % ""
        for xkey in (range (min(x.min(), -radius), max(x.max(), radius)+1)):
            string += "%3i " % xkey

        self.as_ascii = string
        
        return string


    def generate_test_data (self, type='elliptic', invert = False, rot=1):
        """
        Generate a test data set.
        """

        offset_x, offset_y = self.offset
        radius = self.radius
        x    = []
        y    = []
        z    = []
        ones = []
        ii = jj = radius
        #maxval = ii**2 + ii * jj + jj**2 + ii + jj

        ones = []
        for i in range (-radius, radius+1):
            for j in range (-radius, radius+1):
                #  skip if outside circle
                if radius**2 < (i**2 + j**2):
                    continue
                x.append (i + offset_x)
                y.append (j + offset_y)
                # ii = abs(i)
                # jj = abs(j)
                #zval = ii**2 + ii * jj + jj**2 + ii + jj
                #zval = ii * 2 + jj * 2
                zval = self.generate_test_val(x=i, y=j, type = type, rot=rot)
                #zval = maxval - zval
                #zval += random.random()
                z.append (zval)
                #print i**2, i*j, j**2, i, j, zval
        
        #self.ones = np.ones(len(x))
        self.x    = np.array (x)
        self.y    = np.array (y)
        self.z    = np.array (z)
        
        if invert:
            # need testing...
            self.z = np.amax(self.z) - self.z
            # tt = self.get_max_z_coord()
            # max = tt[2]
            # self.z  = max - self.z
        
        return

    def get_max_z_coord (self):
        ii = int (self.z.argmax())
        return self.x[ii], self.y[ii], self.z[ii]

    def get_min_z_coord (self):
        ii = int (self.z.argmin())
        return self.x[ii], self.y[ii], self.z[ii]

    def get_axes (self):
        """
        Get the hyperbolic and elliptic axes.
        """

        B = self.coeffs['B']
        theta = self.theta
        centre_x = self.centre[0]
        centre_y = self.centre[1]

        #  get the axes
        #  formula is '0 = ax + by + c'
        if B == 0:
            a1 =  0
            b1 =  0
            c1 =  centre_y
            a2 =  0
            b2 =  0
            c2 =  centre_x
        else:
            a1 = tan(theta)                  #  x slope
            b1 = -1                          #  y slope
            c1 = centre_y - a1 * centre_x    #  intercept
            
            a2 = tan(theta + pi_div2)        #  x slope
            b2 = -1                          #  y slope
            c2 = centre_y - a2 * centre_x    #  intercept

        self.axes = [[a1, b1, c1], [a2, b2, c2]]

    
    def get_axes_parabolic(self):
        """
        Get the parabolic axis (differs from elliptic and hyperbolic)
        """

        c = self.coeffs
        theta = self.theta
        costheta = cos (theta)

        if c['B'] != 0:
            if c['_A'] == 0:
                a = tan (theta)
                b = -1
                c = -1 * c['_E'] / (2 * c['_C'] * costheta)
            elif c['_C'] == 0:
                a = tan (theta + pi_div2)
                b = -1
                c = -1 * c['_D'] / (2 * c['_A'] * costheta)
        else:
            if c['A'] == 0:
                a =  0
                b = -1
                c =  -1 * c['E'] / (2 * c['C'])
            elif c['C'] == 0:
                a =  0
                b = -1
                c =  -1 * c['D'] / (2 * c['A'])

        self.axes = [[a, b, c], [] ]
        return self.axes

    def get_centre_point (self):
        c = self.coeffs
        A = c['A']
        B = c['B']
        C = c['C']
        D = c['D']
        E = c['E']

        #  get the centre point
        a = np.array([[2*A, B], [B, 2*C]])
        b = np.array([-D, -E])
        xy = np.linalg.solve(a, b)

        centre_x, centre_y = (float("%.14f" % xy[0]), float("%.14f" % xy[1]))
        #print "centre is %.6f, %.6f" % (xy[0], xy[1])
        self.centre = [centre_x, centre_y]

        return self.centre

    def process_elliptic (self):

        try:
            centre_coord = self.get_centre_point()
        except:
            raise
        centre_x = centre_coord[0]
        centre_y = centre_coord[1]
        radius   = self.radius

        dist = sqrt(centre_x**2 + centre_y**2)

        #  get the axis dists also
        axes = self.get_axes()
        dist_axis1 = self.get_dist_p2l (axes[0])
        dist_axis2 = self.get_dist_p2l (axes[1])
        #print "D_ax1 %.3f %.3f %.3f" % (axes[0][0], axes[0][1], dist_axis1)
        #print "D_ax2 %.3f %.3f %.3f" % (axes[1][0], axes[1][1], dist_axis2)

        min_axis_dist = min (dist_axis1, dist_axis2)

        self.dist_ax1 = dist_axis1
        self.dist_ax2 = dist_axis2
        self.dist     = dist

        #  check if we are a peak/pit
        #  if yes then calc muval
        if self.coeffs['_A'] < 0:
            convex = True
            if dist <= radius:
                self.morphclass = 'peak'
                self.muval['peak'] = 1 - dist / radius
            elif min_axis_dist <= radius:
                self.morphclass = 'ridge'
            else:
                self.morphclass = 'plane'

        elif self.coeffs['_A'] > 0:
            convex = False
            if dist <= radius:
                self.morphclass = 'pit'
                self.muval['pit'] = 1 - dist / radius
            if min_axis_dist <= radius:
                self.morphclass = 'valley'
            else:
                self.morphclass = 'plane'
    
        #  get the ridge/valley muvals as well
        if min_axis_dist <= radius:
            if self.morphclass == 'peak' or self.morphclass == 'ridge':
                self.muval['ridge']  = 1 - min_axis_dist / radius
            elif self.morphclass == 'pit' or self.morphclass == 'valley':
                self.muval['valley'] = 1 - min_axis_dist / radius
            else:
                raise 'Unable to process morphclass %s' % self.morphclass
        else:
            self.morphclass = 'plane'
            self.muval['plane'] = 1

        # return

    def process_hyperbolic (self):

        try:
            centre_coord = self.get_centre_point()
        except:
            raise
        centre_x = centre_coord[0]
        centre_y = centre_coord[1]
        radius = self.radius
        dist = sqrt(centre_x**2 + centre_y**2)
        self.dist = dist

        #  now proces the axes for ridge/valley muvals
        axes = self.get_axes()
        if self.coeffs['_A'] > 0:
            convex_axis  = axes[0]
            concave_axis = axes[1]
        else:
            convex_axis  = axes[1]
            concave_axis = axes[0]

        #  does the axis intersect the window?
        #  If dist to axis < radius then yes
        dist_conv_axis = self.get_dist_p2l (convex_axis)
        dist_conc_axis = self.get_dist_p2l (concave_axis)
        #print "D_ax1 %.3f %.3f %.3f" % (axes[0][0], axes[0][1], dist_axis1)
        #print "D_ax2 %.3f %.3f %.3f" % (axes[1][0], axes[1][1], dist_axis2)
        
        min_axis_dist = min (dist_conc_axis, dist_conv_axis)
        self.dist_ax1 = dist_conv_axis
        self.dist_ax2 = dist_conc_axis


        if   dist <= radius:
            self.morphclass = 'saddle'
            self.muval['saddle'] = 1 - dist / radius
        elif min_axis_dist <= radius:
            if dist_conv_axis <= dist_conc_axis:
                self.morphclass = 'ridge'
            else:
                self.morphclass = 'valley'
        else:
            self.morphclass = 'plane'
            self.muval['plane'] = 1
        
        #  get the ridge and valley muvals
        if self.morphclass != 'plane':
            if dist_conv_axis <= radius:
                self.muval['ridge']  = 1 - dist_conv_axis / radius
            if dist_conc_axis <= radius:
                self.muval['valley'] = 1 - dist_conc_axis / radius

        # return
    
    def process_parabolic(self):
        
        #try:
        #    centre_coord = self.get_centre_point()
        #except:
        #    raise
        
        radius = self.radius
        c      = self.coeffs

        #  get the axis dist
        axes = self.get_axes_parabolic()
        dist_axis1 = self.get_dist_p2l (axes[0])
        #print "D_ax1 %.3f %.3f %.3f" % (axes[0][0], axes[0][1], dist_axis1)
        #
        self.dist_ax1 = dist_axis1
        self.dist_ax2 = None
        self.dist     = dist_axis1

        dist = dist_axis1

        #  check if we are a ridge/valley
        #  if yes then calc muval
        if c['_A'] < 0 or c['_C'] < 0:
            convex_up = True
            if dist <= radius:
                self.morphclass = 'ridge'
                self.muval['ridge'] = 1 - dist / radius
            else:
                self.morphclass = 'plane'
                self.muval['plane'] = 1
        else:
            convex_up = False
            if dist <= radius:
                self.morphclass = 'valley'
                self.muval['valley'] = 1 - dist / radius
            else:
                self.morphclass = 'plane'
                self.muval['plane'] = 1

        # return


    def process_planar (self):
        self.morphclass = 'plane'
        self.muval['plane'] = 1

        # return

    def get_cmti(self):
        """
        Calculate the CMTI value.
        This is the most extreme value of the ridge membership
        and the valley membership, where valleyness is negated.
        """

        cmti = None
        if self.muval['ridge'] > self.muval['valley']:
            cmti = self.muval['ridge']
        else:
            cmti = -1 * self.muval['valley']
        self.cmti = cmti
        
        return cmti


    def fit_model (self):
        x = self.x
        y = self.y
        z = self.z
        ones = np.ones(len (z))

        v = np.array([x**2, x * y, y**2, x, y, ones]).T

        model = linalg.lstsq(v, z)

        #  mean squared error
        #MSE = float (model[1]) / len(z)
        try:
            sse = float (model[1])
            MSE = sse / len(z)
            self.r2 = 1 - (MSE / float (np.var(z)))
            #print self.r2
        except:
            if float (np.var(z)) == 0:  #  no variation, perfect model?
                self.r2 = 1
            else:
                self.r2 = None
        #print "variance", np.var(z)
        #print "r^2 = 1 - (%.3f / %.3f) = %.3f" % \
        #        ( model[1] / len (z),
        #         np.var(z),
        #         self.r2
        #        )

        #  snap the vals to 14 decimal place precision
        #  to avoid floating point errors
        labels = ('A', 'B', 'C', 'D', 'E', 'F')
        i = 0
        coeffs = model[0]
        for coeff in (coeffs):
            val = ( float ("%.14f" % coeff))
            self.coeffs[labels[i]] = val
            i += 1

        #print self.coeffs
        return

    def rotate_coeffs(self):
        """
        Rotate the coeffs if needed.
        """

        c     = self.coeffs
        theta = self.theta
        #A, B, C, D, E = c['A', 'B', 'C', 'D', 'E']
        #
        if c['A'] != c['C']:
            #print 'A != C, %f, %f' % (c['A'], c['C'])
            #print (B / (A - C))
            theta = atan (c['B'] / (c['A'] - c['C'])) / 2
        else:
            #print 'A == C, %f, %f' % (c['A'], c['C'])
            theta = pi / 4
        #print "Rotate angle = %f" % theta
        self.theta = theta

        if self.coeffs['B'] == 0:
            c['_A'] = c['A']
            #c['_B'] = c['B']
            c['_C'] = c['C']
            c['_D'] = c['D']
            c['_E'] = c['E']
        else:
            theta2    = 2 * theta
            costheta  = cos(theta)
            sintheta  = sin(theta)
            cos2theta = cos(theta2)
            sin2theta = sin(theta2)

            c['_A'] =    (c['A']+c['C'])/2              \
                      + ((c['A']-c['C'])/2) * cos2theta \
                      +  (c['B']/2)         * sin2theta
            #c['_B'] = 0
            c['_C'] =   (c['A']+c['C'])/2               \
                      + ((c['C']-c['A'])/2) * cos2theta \
                      - (c['B']/2) * sin2theta
            c['_D'] =    c['D'] * costheta              \
                      +  c['E'] * sintheta
            c['_E'] =    c['E'] * costheta              \
                      -  c['D'] * sintheta

        #c['_F'] = c['F']

        return

    def get_morphtype(self):
        A = self.coeffs['A']
        B = self.coeffs['B']
        C = self.coeffs['C']
        
        #  what sort of paraboloid are we?
        morphval = B**2 - 4 * A * C
        
        if A == B == C == 0:
            morphtype = 'planar'
        elif morphval < 0:
            morphtype = 'elliptic'
        elif morphval > 0:
            morphtype = 'hyperbolic'
        else:
            morphtype = 'parabolic'
        #print morphval, morphtype

        #  is the quadric axis parallel to x or y?
        parallel = B == 0
        #print "Parallel? %s" % parallel

        self.parallel  = parallel
        self.morphval  = morphval
        self.morphtype = morphtype

        return

    def get_morphclass(self):
        """Get the morphclass for a function"""
        morphclass = None

        if self.morphtype == 'elliptic':
            self.process_elliptic()
        elif self.morphtype == 'hyperbolic':
            self.process_hyperbolic()
        elif self.morphtype == 'parabolic':
            self.process_parabolic()
        elif self.morphtype == 'planar':
            self.process_planar()
        else:
            raise "Unable to process morphtype %s (morphclass is %s)" \
                    % self.morphclass, self.morphtype

        self.get_cmti()
        
        return self.morphclass

    

if __name__ == "__main__":
    """
    Run the module in test mode.
    """

    radius = 5
    #random.seed (25)
    offsets = ((0,0), (1, 1), ( 2, 0 ), (0, 2), (0, 10), (9, 10))
    inverts = (False, True)
    inverts = (False,)
    types   = ('elliptic', 'hyperbolic', 'parabolic', 'planar', 'flat')
    #types   = ('elliptic', 'hyperbolic',)
    #types   = ('flat',)
    
    rots    = (0, 1, 2)

    self = UNSWDEM (radius=radius)

    for rot in rots:
        for invert in inverts:
            for type in (types):
                for offset in offsets:
                    self.offset = offset
    
                    self.generate_test_data(invert = invert, type = type, rot = rot)
                
                    self.fit_model()
                    self.rotate_coeffs()
                
                    self.get_morphtype()
                    
                    self.get_morphclass()
                    
                    #  test stuff starts here
                
                    if invert:
                        inv_text = 'inverted'
                    else:
                        inv_text = 'not inverted'
                    print "Run: %s %s %.3f %.3f %3f" % (type, invert, offset[0], offset[1], rot)
                    print "Max: ", self.get_max_z_coord()
                    print "Min: ", self.get_min_z_coord()
                
                    cc = {}
                    for key in self.coeffs.keys():
                        cc[key] = "%.3f" % self.coeffs[key]
                    print cc
                    print "R2:          %f" % self.r2
                    try:
                        print "centre:      (%.3f, %.3f)" % (self.centre[0], self.centre[1])
                    except:
                        print "centre:      undefined"
                    print "Parallel?    %s" % self.parallel
                    print "Rotate angle %f" % self.theta
                    print "morphval     %f" % self.morphval
                    print 'morphclass:  %s' % self.morphclass
                    print 'morphtype:   %s' % self.morphtype
                    try:
                        print 'dist is      %f' % self.dist
                    except:
                        print 'dist is undefined'
                    print "axes:        ", self.axes
                    print "dist axis 1: ", self.dist_ax1
                    print "dist axis 2: ", self.dist_ax2
    
                    muvals = ''
                    for key in self.muval.keys():
                        muvals += '%s: %.3f, ' % (key, self.muval[key])
                    print muvals
                    print "CMTI:      %.3f" % self.cmti
                    print ''
                    print self.data_to_ascii()
                    print ''
                    
                    for i in (0, 1):
                        try:
                            axis = self.axes[i]
                            dist = self.get_dist_p2l (axis, x0=offset[0], y0=offset[1])
                            print 'offset dist %i: %.3f' % (i, dist)
                        except:
                            pass
                    print ""
                    self.reset()
    
    print 'End'