##!/usr/bin/env python

"""
Process SRTM DEM data using the UNSWDEM algorithm.

Converts each raster and its neighbours to point format for processing.

Analyses are conducted for a set radius and results saved to a feature class.

Feature classes should then be converted to raster (possibly a multiband)
for storage.

LONGITUDES NEED TO BE IN [-180,180] TO WORK WITH THE SRTM DATA
"""


import arcpy
import os
import sys
import datetime
import re
from unswdem import UNSWDEM
import argparse
# import arcpy.management as arcmgt
# import string
# import time
# from collections import defaultdict

default_coord_sys     = r"WGS 1984"
# default_coord_sys     = r"GCS_WGS_1984"
# default_utm_coord_sys = r"WGS 1984 UTM Zone 1N"
base_utm_coord_sys = 32601

default_sr = arcpy.SpatialReference (default_coord_sys)

regex = r"(?<=PROJCS\[')[.\w]+?(?=')"
re_proj_name  = re.compile(regex)
regex = r"(?<=PARAMETER\['Central_Meridian',).+?(?=\])"
re_cent_merid = re.compile(regex)
regex = r"(?<=PROJCS\[')[.\w]+?(?=')"
re_proj_name = re.compile(regex)
regex = r"(?<=PARAMETER\['Central_Meridian',).+?(?=\])"
re_cent_merid = re.compile(regex)


#test = "PROJCS['WGS_1984_UTM_Zone_1N',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',-177.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]];IsHighPrecision"
#x = re_proj_name.sub  ("BLAH", test)
#y = re_cent_merid.sub ("BLAH", test)
#print x, y

# def val_or_null(value):
#     if value is None:
#         return ""
#     else:
#         return value

class srtm_processor:
    def __init__ (self, raster_workspace, output_workspace, radius_text, outline_polygon,
    globber, overwrite, calibration_location, outline_polygon_res):
        # Set all the properties

        # input from arg
        self.r_workspace      = raster_workspace
        self.out_workspace    = output_workspace
        self.radius_text      = ' '.join(radius_text)
        self.outline_polygon_file = outline_polygon
        self.globber = globber
        self.overwrite_outputs = overwrite
        self.calibration_locations = calibration_location
        self.outline_polygon_res = outline_polygon_res


        self.out_file         = None
        self.current_pt_file  = ''
        self.current_pt_lyr   = ''
        self.current_central_raster   = ''
        self.nbr_files        = set()
        # self.file_name_field  = 'filename'
        self.radius           = 500
        self.radius_units     = "meters"
        self.extent_orig      = 'MAXOF'  #  default extent
        self.unswdem           = UNSWDEM()
        # self.radius_text       = "cause error"  #  break process if not overridden by user

        #  some working layer names
        self.bound_lyr                = 'bound_lyr'
        self.bounds_with_cal_locs_lyr = 'bounds_with_cal_locs_lyr'
        self.current_poly_lyr         = 'current_poly_lyr'

        self.calibration_locations_were_projected = False
        self.project_calibration_locs_file = None

        self.outline_polygon_file  = ''
        self.calibration_locations = ''
        self.create_cal_locs       = False
        self.outline_polygon_res   = 1000

        self.feature_class_names = {}
        self.failed_deletions    = []
        self.scratch_pfx         = ''

        return

    # def update_attr_from_dict (self, dict):
    #     for (key, value) in dict.items():
    #         self.__dict__[key] = value
    #
    #     return

    def process_radius_text (self):
        self.radius = self.radius_text.split(' ')[0]
        self.radius_units = self.radius_text.split(' ')[1]

    def get_feature_class_name (self, raster_name):
        if not raster_name in self.feature_class_names.keys():
            f_name   = os.path.splitext(raster_name)[0] + '_p'  #  handle any extensions
            out_feat = arcpy.CreateScratchName (f_name + self.scratch_pfx, "", 'Shapefile', self.out_workspace)
            self.feature_class_names[raster_name] = out_feat
        return self.feature_class_names[raster_name]

    def get_feature_count (self, layer):
        #print "Getting feature count for layer %s" % layer
        res = arcpy.GetCount_management(layer)
        count = int (res[0])
        return count

    def get_calibration_locations_layer_name (self):
        '''
        if lyr_name exists, do nothing
        otherwise, make a new feature layer from file named lyr_name
        in both cases, return lyr_name
        '''
        lyr_name = 'calibration_locations_lyr'
        if arcpy.Exists (lyr_name):
            return lyr_name
        # file = ""
        if self.create_cal_locs:
            file = self.current_pt_file
        else:
            file = self.calibration_locations
        arcpy.MakeFeatureLayer_management(file, lyr_name)
        return lyr_name

    def calculate_coord_sys (self, layer):
        """
        Calculate the coordinate system for the current raster.
        This is a UTM modified to be centred on the processing raster.
        """
        desc     = arcpy.Describe(layer)
        sr       = desc.SpatialReference

        #  drop out if we are already projected
        if sr.Type == 'Projected':
            return sr.exportToString()

        ext        = desc.extent
        centre_lon = (ext.XMax + ext.XMin) / 2

        #print "centre_lon is %f" % (centre_lon)
        #print ext.Xmax, ext.Xmin
        #print sr.exporttostring()

        #sr1 = arcpy.CreateObject("spatialreference")
        #sr1.CreateFromFile(base_utm_coord_sys)
        sr1 = arcpy.SpatialReference(base_utm_coord_sys)

        #sr1.CentralMeridian = centre_lon

        #  spatial reference methods had no write effect at 9.3, so using regex substitute approach
        #  might be redundant in 10.1
        name = 'SRTM_Processor_UTM_%.1f' % (centre_lon)
        sr_text = sr1.exportToString()
        sr_text = re_proj_name.sub(name, sr_text)
        sr_text = re_cent_merid.sub("%.1f" % centre_lon, sr_text)
        self.coord_sys = sr_text.replace("'", '"')

        return sr_text

    def create_calibration_point_data (self, raster_name):
        self.create_point_data ([raster_name])

        return


    def get_raster_nbrs_to_use(self, raster_name):
        #  identify the neighbouring files we need
        # self.nbr_files = {}

        bound_lyr = self.bound_lyr
        cal_lyr   = self.get_calibration_locations_layer_name()
        #query = "%s = '%s'" % (self.file_name_field, raster_name)
        #arcmgt.SelectLayerByAttribute(bound_lyr, "NEW_SELECTION", query)
        #arcmgt.SelectLayerByLocation (bound_lyr, "INTERSECT", bound_lyr, 0, "ADD_TO_SELECTION")
        #  add the relevant nbrs (bound_lyr should be defined to include only the relevant records)
        arcpy.SelectLayerByLocation_management(bound_lyr, "INTERSECT", cal_lyr, self.radius_text, "NEW_SELECTION")

        #  get some summary stuff from the selected features
        # not used... commented out
        # count = int(arcpy.GetCount_management(bound_lyr)[0])
        # res = arcmgt.GetCount(bound_lyr)
        # count = int (res.getOutput(0))
        subrows = arcpy.da.SearchCursor(bound_lyr, 'filename')
        for subrow in subrows:
            self.nbr_files.add(subrow[0])
        # subrow  = subrows.next()
        # while subrow:
        #     self.nbr_files.append (subrow.getValue (self.file_name_field))
        #     subrow = subrows.next()
        #
        # self.nbr_files = self.get_unique_values (self.nbr_files)
        count = len(self.nbr_files)

        print raster_name, count, self.nbr_files

        del subrows

        return self.nbr_files


    def create_point_data (self, raster_names):

        for raster_name in raster_names:
            try:
                # Set local variables
                out_feat  = self.get_feature_class_name (raster_name)
                # dir       = os.path.dirname(out_feat)
                basename      = os.path.basename(out_feat)
                name = os.path.splitext(basename)[0]
                # name      = string.replace(name, ".shp", "")

                if arcpy.Exists (out_feat):
                    continue

                print "Generating point data %s" % out_feat
                print "Extent is %s" % arcpy.env.extent
                try:
                    raster = os.path.join (self.r_workspace, raster_name)
                    arcpy.RasterToPoint_conversion(raster, out_feat)
                except arcpy.ExecuteError:
                    msg = arcpy.GetMessages(2)
                    #print msg
                    if msg.startswith('ERROR 010151'):
                        pass  #  possibly a bad thing?
                    else:
                        raise
                except:
                    raise

                sr   = arcpy.Describe(out_feat).SpatialReference
                count = int(arcpy.GetCount_management(out_feat)[0])

                # res = arcpy.GetCount_management(out_feat)
                # count = int (res.getOutput(0))

                #  project if needed
                if count and sr.Type != 'Projected':
                    out = self.scratch_pfx + name + "prjd"
                    print 'Projecting data as %s' % (out)
                    projected = arcpy.CreateScratchName(out, "", "Shapefile", dir)

                    #  set coord sys based on the current central raster
                    coord_sys = self.calculate_coord_sys(self.current_central_raster)
                    #print coord_sys

                    #  now project it into our desired coord sys
                    arcpy.Project_management(out_feat, projected, coord_sys)

                    #  and lay the cuckoo's egg (or just rename it)
                    try:
                        arcpy.Delete_management(out_feat)
                        arcpy.Rename_management(projected, out_feat)
                    except:
                        self.feature_class_names[raster_name] = projected
                        self.failed_deletions.append(out_feat)

                #  ensure we have a spatial index
                arcpy.AddSpatialIndex_management(out_feat)

            except:
                raise

        return

    def cleanup (self):
        """
        Cleanup files and records specific to this run.
        Keep a track of those that failed as we might be able to clear them later.
        """
        lyr_name = self.get_calibration_locations_layer_name()
        if arcpy.Exists (lyr_name):
            arcpy.Delete_management(lyr_name)

        self.failed_deletions.extend (self.delete_files (self.feature_class_names.values()))
        self.feature_class_names.clear()

        self.failed_deletions = self.delete_files (self.failed_deletions)

        self.reset_arcpy_extent()

        return

    def delete_files(self, files):
        #print files
        not_deleted = []
        for out_feat in files:
            #out_feat = self.get_feature_class_name (raster_name)
            if arcpy.Exists (out_feat):
                try:
                    arcpy.Delete_management(out_feat)
                    #print "deleted %s" % out_feat
                except:
                    print "unable to delete %s" % out_feat
                    not_deleted.append(out_feat)
                    pass

        return not_deleted

    # @profile
    def run_analysis (self):

        start_time =  datetime.datetime.now()
        print "Start time: %s" % start_time

        unswdem        = self.unswdem
        unswdem.radius = float (self.radius)

        oldext = arcpy.env.extent  #  override the extent, because we are losing points otherwise
        arcpy.env.extent = None

        #  generate some layers
        lyrs = []
        for file in self.nbr_files:
            point_file = self.get_feature_class_name(file)
            lyr = point_file + "_lyr"
            if not arcpy.Exists (lyr):
                arcpy.MakeFeatureLayer_management(point_file, lyr)
                lyrs.append (lyr)

        #  set the analysis coord system to the first layer
        self.set_arcpy_coordsys(lyrs[0])

        #print "Current point file is %s" % self.current_pt_file
        #  need to project the calibration points layer to the same as the analysis layers
        #  this should maintain the selected set
        calibration_points_layer = self.get_projected_calibration_locations_layer_name()  #  REDUNDANT???

        if calibration_points_layer is None:
            print "Skipping, no features in this tile overlap with the DEM"
            return

        desc = arcpy.Describe(calibration_points_layer)
        shape_field_name = desc.ShapeFieldName
        OID_field_name   = desc.OIDFieldName

        current_cal_pt_lyr = "current_pt_lyr_c"
        arcpy.MakeFeatureLayer_management(calibration_points_layer, current_cal_pt_lyr)


        total_rows = self.get_feature_count (calibration_points_layer)
        if total_rows == 0:
            print "Skipping, no features in this tile overlap with the DEM"
            return

        print "Processing %d locations (%s)" % (total_rows, total_rows)

        points = arcpy.SearchCursor(calibration_points_layer)

        j = 1
        for point_row in points:
            if j == total_rows or (j % 50 == 0):
                feedback = "\b" * len(str(j-1)) + ("%s" % j)
                sys.stdout.write (feedback)

            if max_j is not None and j >= max_j:
                break

            #if j % 1000 == 0:
            #    #  remake the current point layer
            #    arcpy.delete (current_pt_lyr)
            #    arcpy.makefeaturelayer(calibration_points_layer, current_pt_lyr)

            OID  = point_row.getValue(OID_field_name)
            feat = point_row.getValue(shape_field_name)
            #if need_to_project_pts:
            #    feat = feat.projectAs(analysis_coord_sys)
            this_pnt  = feat.firstPoint

            if this_pnt is None:
                continue

            x_cen = this_pnt.X
            y_cen = this_pnt.Y

            x = []
            y = []
            z = []


            projected_pnt = feat.projectAs(default_sr)
            proj_pnt = projected_pnt.firstPoint

            #  track points we already have
            gottem = {}

            #  now select all the relevant points in the other files,
            #  and then loop over them to get their coords and values
            for lyr in lyrs:
                arcpy.SelectLayerByLocation_management(lyr, "WITHIN_A_DISTANCE", projected_pnt, self.radius_text)
                #  skip this count check - profiling shows it takes too long
                # count = self.get_feature_count(lyr)
                # if not count:
                #     continue

                sel_rows = arcpy.da.SearchCursor (
                    lyr,
                    [OID_field_name, shape_field_name, 'GRID_CODE']
                    )
                for sel_row in sel_rows:
                    feat = sel_row[1]
                    xval = feat[0]
                    yval = feat[1]
                    zval = sel_row[2]

                    #  don't add a point we already have from another layer
                    #  round off to nearest unit (should be metres, should never be degrees)
                    if len(lyrs) > 1:
                        key = "%.0f:%.0f" % (xval, yval)
                        if key in gottem:
                            gottem[key] += 1
                            continue
                        else:
                            gottem[key] = 1

                    x.append (xval)
                    y.append (yval)
                    z.append (zval)

                #  cleanup
                del sel_rows

            #  use the projected points here
            data = [OID, proj_pnt.X, proj_pnt.Y, len(x)]

            #  null results if no records
            if len (x) == 0:
                data.extend (("",) * 19)
            else:
                unswdem.set_data(x, y, z)
                unswdem.centre_coords (x_centre = x_cen, y_centre = y_cen)
                #print "Radius is %s" % self.radius
                num_fmt  = "%.3f"
                num_fmtg = "%.10g"

                unswdem.fit_model()
                if unswdem.r2 is None:  #  no valid model for some reason
                    data.extend (("",) * 19)
                else:
                    unswdem.rotate_coeffs()

                    unswdem.get_morphtype()

                    unswdem.get_morphclass()

                    data.extend ([
                        num_fmt % unswdem.r2,
                        unswdem.morphclass,
                        unswdem.morphtype,
                        ])
                    for coeff in ('A', 'B', 'C', 'D', 'E', 'F'):
                        data.append("%.10f" % unswdem.coeffs[coeff])
                    for d in (unswdem.dist, unswdem.dist_ax1, unswdem.dist_ax2):
                        data.append ("%.3f" % d)
                    for key in unswdem.get_morphclasses():
                        data.append(num_fmt % unswdem.muval[key])
                    data.append(num_fmt % unswdem.get_cmti())

            text = ",".join ([("%s" % k) for k in data])
            #print text
            self.out_file.write (text + "\n")

            unswdem.reset()

            j += 1

        feedback   = "\b" * len(str(j-1)) + ("%s" % j)
        sys.stdout.write (feedback)

        sys.stdout.write ("\n")

        #  clean up the cursor
        del points

        #  cleanup layers to free memory
        del_lyrs = {current_cal_pt_lyr: 1}
        if self.calibration_locations_were_projected:
            del_lyrs[calibration_points_layer] = 1
        for lyr in lyrs:
            del_lyrs[lyr] = 1
        for lyr in del_lyrs.keys():
            try:
                arcpy.Delete_management(lyr)
            except:
                print "Unable to delete layer %s" % current_cal_pt_lyr
                pass
        if self.project_calibration_locs_file:
            try:
                arcpy.Delete_management(self.project_calibration_locs_file)
            except:
                print "Unable to delete temp file %s" % self.project_calibration_locs_file

        end_time = datetime.datetime.now()
        print "Elapsed time: %s" % (end_time - start_time)

        unswdem.reset()

        arcpy.env.extent = oldext

        self.reset_arcpy_coordsys()

        return

    def get_header (self):
        header = ['OID',
                  'x', 'y',
                  'n',
                  'r2',
                  'morphclass',
                  'morphtype',
                  'A', 'B', 'C', 'D', 'E', 'F',
                  'dist', 'dist_ax1', 'dist_ax2',
                  ]
        header.extend(self.unswdem.get_morphclasses())
        header.append ("CMTI")

        return ','.join(header)

    # def parse_rest_of_args(self, start = 3):
    #     valid_arg_names = ['radius_text',
    #                        'outline_polygon_file',
    #                        'globber',
    #                        'calibration_locations',
    #                        'overwrite_outputs',
    #                        'outline_polygon_res',
    #                        ]
    #
    #     arg_names  = [sys.argv[x] for x in range (start,     len (sys.argv), 2)]
    #     arg_values = [sys.argv[x] for x in range (start + 1, len (sys.argv), 2)]
    #     arg_dict = dict (zip (arg_names, arg_values))
    #
    #     invalid = []
    #     for name in arg_names:
    #         if not name in valid_arg_names:
    #             invalid.append(name)
    #     if len (invalid):
    #         arcpy.AddError("Invalid arguments found: %s" % invalid)
    #         arcpy.AddMessage(Usage())
    #         print arcpy.GetMessages()
    #         sys.exit()
    #
    #     self.update_attr_from_dict(arg_dict)
    #
    #     if len(self.calibration_locations) == 0:
    #         self.create_cal_locs = True
    #     # process the overwrite - clunky but avoids '0' being true
    #     self.overwrite_outputs   = eval (str(self.overwrite_outputs))
    #     self.outline_polygon_res = eval (str(self.outline_polygon_res))
    #     return

    def create_bounds_file (self):
        """Create the bounds file if needed."""

        if not arcpy.Exists (self.outline_polygon_file):
            print "Creating bounds polygon file %s" % self.outline_polygon_file
            import get_outlines
            self.outline_polygon_file = get_outlines.get_outlines(
                self.globber,
                self.outline_polygon_file,
                self.r_workspace,
                self.outline_polygon_res,
                )
            print 'Polygon bounds file is %s' % self.outline_polygon_file
        return

    def make_working_layers (self):
        #  create a bounds layer from which we will select those which we overlap
        #  (if there is a calibration file supplied)
        arcpy.MakeFeatureLayer_management(self.outline_polygon_file, self.bounds_with_cal_locs_lyr)
        if not self.create_cal_locs:
            cal_lyr = self.get_calibration_locations_layer_name()
            arcpy.SelectLayerByLocation_management(self.bounds_with_cal_locs_lyr, "CONTAINS", cal_lyr)
            print "Working with %s polygons" % self.get_feature_count (self.bounds_with_cal_locs_lyr)

        #  generate the bounds layer to use as a working set
        arcpy.MakeFeatureLayer_management(self.outline_polygon_file, self.bound_lyr)

        #  now create the polygon layer which we will use as a working set
        arcpy.MakeFeatureLayer_management(self.outline_polygon_file, self.current_poly_lyr)

    def store_arcpy_extent (self):
        self.extent_orig = arcpy.env.extent

    def reset_arcpy_extent (self):
        arcpy.extent = self.extent_orig
        print "Extent reset to %s" % arcpy.extent

    def set_arcpy_extent_from_selection (self, layer, query):
        """Incomplete."""
        tmp = 'tmp_set_arcpy_extent_from_selection'
        arcpy.MakeFeatureLayer_management(layer, tmp, query)
        dsc = arcpy.Describe (tmp)
        ext = dsc.extent
        extent = "%s %s %s %s" % (
            ext.XMin,
            ext.YMin,
            ext.XMax,
            ext.YMax,
            )
        arcpy.env.extent = extent

        print "Extent is now %s (%f x %f)" % (
            arcpy.extent,
            ext.XMax - ext.XMin,
            ext.YMax - ext.YMin,
            )

        arcpy.Delete_management(tmp)

        return extent

    def set_arcpy_extent_buffered (self, layer):
        buffname = arcpy.CreateScratchName (self.scratch_pfx + "xbuff", "", "Shapefile")
        arcpy.Buffer_analysis(layer, buffname, self.radius_text)
        dsc = arcpy.Describe (buffname)
        #  NEED TO THROW AN EXCEPTION IF NOTHING WAS SELECTED
        #count = int (str (arcmgt.GetCount(buffname)))
        #if count == 0:
            #raise "No features selected"

        ext = dsc.extent
        extent = "%s %s %s %s" % (
            ext.XMin,
            ext.YMin,
            ext.XMax,
            ext.YMax,
            )
        arcpy.env.extent = extent

        print "Extent has been buffered to %s (%f x %f)" % (
            arcpy.env.extent,
            ext.XMax - ext.XMin,
            ext.YMax - ext.YMin,
            )

        try:
            arcpy.Delete_management(buffname)
        except:
            pass

        return extent

    def set_arcpy_coordsys (self, layer):
        self.coord_sys_orig = arcpy.env.outputCoordinateSystem

        #desc = arcpy.describe (layer)
        #sr   = desc.SpatialReference.exporttostring()

        sr = self.calculate_coord_sys(layer)
        arcpy.env.outputCoordinateSystem = sr
        #  feedback
        sref = arcpy.env.outputCoordinateSystem
        #print "Set coord sys to %s" % sref.name
        return

    def reset_arcpy_coordsys (self):
        arcpy.env.outputCoordinateSystem = self.coord_sys_orig

        #  feedback
        sref = arcpy.env.outputCoordinateSystem
        if sref is None:
            print "Set coord sys to None"
        else:
            print "Set coord sys to %s" % sref.name
        return

    def get_need_to_project_calibration_locations(self):
        lyr = self.get_calibration_locations_layer_name()
        sr_lyr = desc.SpatialReference
        sr_env = arcpy.env.outputCoordinateSystem

        #  if they don't agree then we need to project
        return sr_lyr.exportToString() != sr_env.exportToString()


    def get_projected_calibration_locations_layer_name(self):
        lyr = self.get_calibration_locations_layer_name()

        if self.get_feature_count (lyr) == 0:
            return None  #  should throw an informative exception

        desc = arcpy.Describe(lyr)
        sr_lyr = desc.SpatialReference
        sr_env = arcpy.env.outputCoordinateSystem

        self.calibration_locations_were_projected = False


        #  they don't agree then project
        if sr_lyr.exportToString() != sr_env.exportToString():
            oldext = arcpy.env.extent
            arcpy.env.extent = None
            name = arcpy.CreateScratchName (self.scratch_pfx + "xcalproj_", "", "shapefile")
            #print "projecting calibration points into %s" % name

            try:
                arcpy.Project_management(lyr, name, sr_env)
            except Exception as e:
                if e.message.find('invalid extent for output coordinate system'):
                    print "No calibration points within %s of DEM cells" % self.radius_text
                    print desc
                    arcpy.env.extent = oldext
                    return None
                print e
                raise

            # count_new, count_orig and count_lyr are not used.
            # count_new = int(arcpy.GetCount_management(name)[0])
            # res = arcmgt.GetCount(name)
            # count_new  = int (res.getOutput(0))
            # count_orig = int(arcpy.GetCount_management(lyr)[0])
            # res = arcmgt.GetCount(lyr)
            # count_orig = int (res.getOutput(0))
            new_lyr = name + '_lyr'
            arcpy.MakeFeatureLayer_management(name, new_lyr)
            self.calibration_locations_were_projected = True
            self.project_calibration_locs_file = name
            # count_lyr = int(arcpy.GetCount_management(new_lyr)[0])
            # res = arcmgt.GetCount(new_lyr)
            # count_lyr = int (res.getOutput(0))
            arcpy.env.extent = oldext
            #count_lyr = int (str (arcmgt.GetCount(new_lyr)))
            return new_lyr

        return lyr
    #
    # def get_unique_values (self, seq):
    #     """Get the set of unique vals in a list"""
    #     uset = set(seq)
    #     return list(uset)


def Usage ():
    usage = """
%s <raster workspace> <output workspace>
    radius_text <analysis radius and units>
    outline_polygon_file {polygon feature class}
    globber {file glob pattern}
    calibration_locations {point feature class}
    overwrite_outputs {boolean}
    outline_polygon_res {1000} (in cell units)
    """ \
    % (sys.argv[0])

    return usage



def get_args():
    """
    %s <raster workspace> <output workspace>
        radius_text <analysis radius and units>
        outline_polygon_file {polygon feature class}
        globber {file glob pattern}
        calibration_locations {point feature class}
        overwrite_outputs {boolean}
        outline_polygon_res {1000} (in cell units)
        """
    parser = argparse.ArgumentParser(
        description='In order to make it work, you need to input at least raster_workspace, '
         'output_workspace and radius text, separated by space. The optional arguments are input with flags.\n'
        'Always input positional arguments first with the specific order and then follow the optional arguments which have flags but no orders',
        epilog='for instance the command: "C:\unswdem\data C:\unswdem\data 500 meters -g *.tif -ow",\n'
               'defines raster and output workspace as "C:\unswdem\data", the analysis will use a radius of 500 meters'
               'applied to all .tif files in the workspace, '
               'overwriting outputs if needed. ')
    parser.add_argument('r_workspace', type=str, help='input raster workspace')
    parser.add_argument('out_workspace', type=str, help='output workspace')
    parser.add_argument('radius_text', type=str, nargs=2, help='analysis radius and units')
    parser.add_argument('-p', '--outline_polygon_file', type=str, help='outline polygon feature class')
    parser.add_argument('-g', '--globber', type=str, help='raster file glob pattern')
    parser.add_argument('-c','--calibration_locations', type=str, help='calibration locations: point feature class', default=None)
    parser.add_argument('-ow', '--overwrite', help='use this flag if you want to overwrite', default=False, action='store_true')
    parser.add_argument('-r', '--outline_polygon_res', type=float, default=1000, help='outline polygon resolution')
    #parse
    args = parser.parse_args()
    # access
    raster_workspace = os.path.abspath(args.r_workspace)
    output_workspace = os.path.abspath(args.out_workspace)
    radius_text = args.radius_text
    outline_polygon = args.outline_polygon
    globber = args.globber
    overwrite = args.overwrite
    calibration_locations = args.calibration_locations
    outline_polygon_res = args.outline_polygon_res
    print('workspace is', raster_workspace)
    return raster_workspace, output_workspace, radius_text, outline_polygon, globber, overwrite, calibration_locations, \
           outline_polygon_res


if __name__ == "__main__":
    raster_workspace, output_workspace, radius_text, outline_polygon, \
    globber, overwrite, calibration_locations, outline_polygon_res = get_args()

    max_i = None
    max_j = None

    try:
        #  srtm processor and geoprocessor objects
        srtm = srtm_processor(raster_workspace, output_workspace, radius_text, outline_polygon,
    globber, overwrite, calibration_locations, outline_polygon_res)
        if not srtm.calibration_locations:
            srtm.create_cal_locs = True

        # if len (sys.argv) == 1:
        #     print Usage()
        #     sys.exit()
        #
        # self.r_workspace      = os.path.abspath (arcpy.GetParameterAsText(0))
        # self.out_workspace    = os.path.abspath (arcpy.GetParameterAsText(1))
        # self.parse_rest_of_args()

        srtm.store_arcpy_extent()
        srtm.process_radius_text()

        #  set the workspace
        if not arcpy.env.workspace:
            #arcpy.workspace = self.r_workspace
            arcpy.env.workspace = srtm.out_workspace
        print "Workspace is", arcpy.env.workspace

        #  do we need to create the bounds file?
        #  Can happen if we have a single DEM
        srtm.create_bounds_file()

        srtm.make_working_layers()

        current_poly_lyr = srtm.current_poly_lyr
        desc = arcpy.Describe(current_poly_lyr)
        shape_field_name = desc.ShapeFieldName
        OID_field_name   = desc.OIDFieldName

        poly_rows = arcpy.SearchCursor(srtm.bounds_with_cal_locs_lyr)

        i = 0
        for poly_row in poly_rows:
            i += 1
            # max_i is always none???
            if max_i and i >= max_i:
                print "Maximum polygon count processed, exiting"
                break

            raster_name = poly_row.getValue('filename')
            srtm.current_central_raster = os.path.join (srtm.r_workspace, raster_name)
            s_pfx = 'z' + str (poly_row.getValue(OID_field_name))
            srtm.scratch_pfx = s_pfx
            r_name = os.path.splitext(raster_name)[0]

            file_pfx = '%s_%s_%s_unswdem_%s' % (
                os.path.join (srtm.out_workspace, r_name),
                srtm.radius,
                srtm.radius_units,
                srtm.scratch_pfx,
                )
            filename = file_pfx + '.txt'

            #  skip if we already have this one
            #  should set a rerun flag
            if os.path.exists(filename) and not srtm.overwrite_outputs:
                print "Output %s exists, skipping this tile" % filename
                i += 1
                continue

            print "Processing output %s" % filename

            f = open(filename, 'w')
            srtm.out_file = f
            srtm.out_file.write(srtm.get_header() + "\n")


            query = '"%s" = %d' % (OID_field_name, poly_row.getValue(OID_field_name))
            arcpy.SelectLayerByAttribute_management(current_poly_lyr, "NEW_SELECTION", query)
            # extent changed
            srtm.set_arcpy_extent_buffered(current_poly_lyr)

            #  create the point data set (if needed)
            if srtm.create_cal_locs:
                srtm.create_calibration_point_data(raster_name)

            srtm.current_pt_file = srtm.get_feature_class_name (raster_name)

            pt_count = 1
            if srtm.create_cal_locs:  #  bodge?
                oldext = arcpy.env.extent  #  override the extent, because we are losing points otherwise
                arcpy.env.extent = None
                pt_count = int(arcpy.GetCount_management(srtm.current_pt_file)[0])
                # res = arcpy.GetCount_management(srtm.current_pt_file)
                # pt_count = int(res.getOutput(0))
                arcpy.env.extent = oldext

            arcpy.env.extent = None  #  DEBUG
            cal_lyr = srtm.get_calibration_locations_layer_name()
            arcpy.SelectLayerByLocation_management(cal_lyr, "WITHIN", current_poly_lyr)
            cal_count = srtm.get_feature_count(cal_lyr)
            print "Operating on %i locations" % cal_count
            srtm.set_arcpy_extent_buffered(cal_lyr)

            #  run the analysis if there were points to use
            if pt_count and cal_count:
                # create the neighbouring point data
                srtm.create_point_data(srtm.get_raster_nbrs_to_use(raster_name))

                #  and the projection file for this output
                proj_filename = file_pfx + '.prj'
                pf = open (proj_filename, 'w')
                pf.write (default_sr.exportToString())
                pf.close()

                #  now do stuff

                srtm.run_analysis()

            print 'reset'
            srtm.reset_arcpy_extent()  #  set extent back

            print 'now clean up'
            srtm.cleanup()

            print 'close the file'
            srtm.out_file.close()


        del poly_rows

        print 'Finished'
        sys.exit()

    except:
        try:
            print arcpy.GetMessages()
        except:
            pass
        raise
